import {useEffect, useState} from "react";
import axios from "axios";

export function dataFromServer() {
    const API = 'http://contest.elecard.ru/frontend_data/catalog.json'
    const [card, setCard] = useState([]);
    const [isLoading, setLoading] = useState(false);


    useEffect(() => {
        setLoading(!isLoading)

        try {
            axios.get(API).then(res => {
                setCard(res.data)
            })

        } finally {
            setLoading(false);
        }
    }, [])


    return card
}