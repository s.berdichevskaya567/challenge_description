export function formatDate(unix_timestamp) {

    const monthName = ['Января', 'Февраля', 'Марта',
        'Апреля', 'Мая', 'Июня',
        'Июля', 'Августа', 'Сентября',
        'Октября', 'Ноября', 'Декабря'
    ]

    const date = new Date(unix_timestamp);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const time = day + ' ' + (monthName[month]) + ' ' + year + ' года';

    return time
}