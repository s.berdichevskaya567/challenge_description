import React from 'react'
import './App.css'
import Component from "./app/page/component.jsx";
import {Route, Routes} from "react-router-dom";

function App() {

    return (
        <>
            <Routes>
                <Route path='/' element={<Component/>}/>
            </Routes>

        </>
    )
}

export default App
