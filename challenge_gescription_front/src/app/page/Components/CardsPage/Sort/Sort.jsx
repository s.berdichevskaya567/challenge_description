import React, {useState} from 'react';
import {categories} from "./Category.jsx";
import {split} from "../../../../../data/split.js";

const Sort = ({onChange, card}) => {

    let nameCategory = categories()

    function sorting(fieldName) {
        return (el1, el2) => el1[fieldName] > el2[fieldName] ? 1 : -1;
    }

    function sortingName() {
        return (el1, el2) => split('/', el1.image) > split('/', el2.image) ? 1 : -1;
    }

    const [select, setSelect] = useState('Все');

    const handleChange = (event) => {
        onChange(event.target.name)
        setSelect(event.target.name)
        switch (event.target.name) {
            case 'По категории': {
                card.sort(sorting('category'))
            }
                break
            case 'По дате': {
                card.sort(sorting('timestamp'))
            }
                break
            case 'По названию': {
                card.sort(sortingName())
            }
                break
            case 'По размеру': {
                card.sort(sorting('filesize'))
            }
                break
        }
    }

    return (
        <div >
            <div className='sort sortTitle'>Сортировка</div>
            <div className=' sortCategory'>
                {
                    nameCategory.map((el, index) => {

                        return (
                            <div className='filters' key={index}>
                                <input id={el.key} type={"radio"} name={el.name}
                                       checked={select === `${el.name}`}
                                       onChange={handleChange}
                                />
                                <label className={select === `${el.name}`? 'active' : ''} htmlFor={el.key}>{el.name}</label>
                            </div>
                        )

                    })
                }
            </div>
        </div>
    );
};

export default Sort;