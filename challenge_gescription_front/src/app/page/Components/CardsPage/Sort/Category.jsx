export function categories () {
    const category = [
        {key: 'category', name: 'По категории'},
        {key: 'timestamp', name: 'По дате'},
        {key: 'name', name: 'По названию'},
        {key: 'filesize', name: 'По размеру'}
    ]
    return category
}