import React from 'react';
import '../../../../../App.css'
import Sort from "../Sort/Sort.jsx";

const HeaderCard = ({card, onChange}) => {
    const handleChangeSelected = (value) => {
        onChange(value)
    }

    return (
        <div className='header'>
            <Sort onChange={handleChangeSelected} card={card}/>
        </div>
    );
};

export default HeaderCard;