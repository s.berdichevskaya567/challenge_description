import React from 'react';
import '../../../../../App.css'
import Pagination from "../Pagination/component.jsx";

const FooterCard = ({onClick, page, end}) => {

    return (
        <div className='footer'>
            <Pagination onClick={onClick} page={page} end={end}/>
        </div>
    );
};
export default FooterCard;