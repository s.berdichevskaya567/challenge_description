import React from 'react';

const Pagination = ({onClick, page, end}) => {

    const handlePagination = (value) => {
        onClick(value)
    }

    return (
        <div className='pagination'>
            <button onClick={()=>{handlePagination(1)}}>В начало</button>
            {1}
            <button
                onClick={() => {
                    handlePagination(page - 1)
                    if (page - 1 < 1) {
                        handlePagination(1)
                    }

                }}> &#8592;
            </button>
            {page}
            <button onClick={() => {
                handlePagination(page + 1)
                if (page + 1 > end) {
                    handlePagination(end)
                }

            }}> &#8594;
            </button>
            {end}
            <button onClick={()=>{handlePagination(end)}}>В конец</button>
        </div>
    );
};

export default Pagination;