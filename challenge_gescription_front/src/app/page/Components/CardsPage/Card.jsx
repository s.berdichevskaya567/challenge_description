import React, {useState} from 'react';
import {formatDate} from "../../../../data/formateDate.js";
import {split} from "../../../../data/split.js";
import '../../../../App.css'

const Card = ({oneCardInfo, onClick}) => {

    let [active, setActive] = useState(false)

    const handleActivityState = (value) => {
            onClick(value)
    }

    const cardClassName = ['card']
    if (active) cardClassName.push("cardActive")

    const handleAnimationEnd = (e) => {
        if (e.animationName === 'del') {
            localStorage.setItem(`${oneCardInfo.image}`, JSON.stringify(oneCardInfo))
            handleActivityState(localStorage.length)
            setActive(false)
        }
    }


    return (
            <div className={active ? cardClassName.join(" ") : 'card'}
                 onAnimationEnd={handleAnimationEnd}
            >
                <button onClick={() => {
                    setActive(true)
                }}>
                    х
                </button>
                <div>
                    <img
                        src={`http://contest.elecard.ru/frontend_data/${oneCardInfo.image}`}
                    />
                    <div className='text'>
                        <p> Название файла: {split('/', oneCardInfo.image)}</p>
                        <p> Размер файла: {(Number(oneCardInfo.filesize) / 1000).toFixed(2)} кБ</p>
                        <p> Дата создания файла: {formatDate(oneCardInfo.timestamp)}</p>
                        <p> Категория файла: {oneCardInfo.category}</p>
                    </div>
                </div>
            </div>
    )
};

export default Card;