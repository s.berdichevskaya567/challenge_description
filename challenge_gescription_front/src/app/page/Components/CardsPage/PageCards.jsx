import React, {useEffect, useState} from 'react';
import '../../../../App.css'
import {dataFromServer} from "../../../../data/requier.js";
import Card from "./Card.jsx";
import HeaderCard from "./components/Header.jsx";
import FooterCard from "./components/Footer.jsx";


const PageCards = () => {

    const card = dataFromServer()

    const [page, setPage] = useState(1)
    const [arrayDeletedCards, setArrayDeletedCards] = useState(localStorage.length)
    let end
    if(card.length>0)
    {
       end = Math.ceil((card.length-localStorage.length) / 12)
    }
    const [selected, setSelected] = useState('Все');



    const handleChangeRemovingCards = (value) => {
        setArrayDeletedCards(value)
        return arrayDeletedCards
    }

    useEffect(()=>{
        if (page > end) {
            setPage(end)
        }
    }, [page, end])

    const handlePagination = (value) => {
        setPage(value)
        if (page > end) {
            setPage(end)
        }
    }
    const handleChangeSelected = (value) => {
        setSelected(value)
        return selected
    }



    return (
        <div>
            <HeaderCard onChange={handleChangeSelected} card={card}/>
            <button className='allCard allCardButton'
                    onClick={() => {
                        localStorage.clear()
                        handleChangeRemovingCards(localStorage.length)
                    }}
            >
                Вывести все карточки
            </button>
            <div className='card_area'>
                {
                    card.length > 0 ?
                        card.filter(i => (localStorage.getItem(`${i.image}`) === null)).
                        slice((page - 1) * 12, (page - 1) * 12 + 12).
                        map((el, index) => {
                            return (
                                <div key={index}>
                                    <Card oneCardInfo={el} onClick={handleChangeRemovingCards}/>
                                </div>
                            )
                        })
                        :
                        <div className='lodging'>...ЗАГРУЗКА...</div>
                }
            </div>
            <FooterCard onClick={handlePagination} page={page} end={end}/>
        </div>
    );
};

export default PageCards;