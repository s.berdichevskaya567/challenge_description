import React, {useState} from 'react';
import Modal from "./Modal.jsx";
import {split} from "../../../../data/split.js";
import {formatDate} from "../../../../data/formateDate.js";

const LevelTwo = ({card, el}) => {

    let [active, setActive] = useState([]);
    const [modalActive, setModalActive] = useState(false)
    const [valueImg, setValueImg] = useState()
    const [valueSize, setValueSize] = useState()
    const [valueDate, setValueDate] = useState()
    const [open, setOpen] = useState([])

    const changeOfActivity = (e) => {
        if(active.includes(e.currentTarget.dataset.id) === false)
        {
            let copy = Object.assign([], active)
            copy.push(e.currentTarget.dataset.id)
            setActive(copy)
            setOpen(copy)
        } else {
            setActive(active.filter((el) => el != e.currentTarget.dataset.id))
            setOpen(active.filter((el) => el != e.currentTarget.dataset.id))
        }
    };


    return (
        <>
            {
                card.filter(i => (el === i.category)).map((ell, index) => {
                    return (
                        <div key={index} style={{wordWrap: "break-word"}}>
                            <div className="treeListNameCard" style={{display: 'inline-block'}} key={ell.image} data-id={index} onClick={changeOfActivity}>
                                {open.includes(index.toString()) && open ? <> &#9660;  </> : <> &#9658;  </>}
                                {split('/', `${ell.image}`)}
                                {active.includes(index.toString()) &&
                                    <div className="thumbnail">

                                        <img  src={`http://contest.elecard.ru/frontend_data/${ell.image}`}
                                              onClick={() => {
                                                  setModalActive(true),
                                                      setValueImg(ell.image),
                                                      setValueSize(ell.filesize),
                                                      setValueDate(ell.timestamp)
                                              }}
                                        />
                                        <div style={{paddingLeft: "10px", fontSize: "14px"}}>
                                            {`${(Number(ell.filesize) / 1000).toFixed(2)} кБ`}
                                        </div>
                                        <div style={{paddingLeft: "10px", fontSize: "14px"}}>
                                            {`${(formatDate(ell.timestamp))}`}
                                        </div>



                                    </div>
                                }
                            </div>
                        </div>
                    )
                })
            }
            <Modal active={modalActive} setActive={setModalActive} nameImg={valueImg} sizeImg={valueSize}/>

        </>

    );
};

export default LevelTwo;