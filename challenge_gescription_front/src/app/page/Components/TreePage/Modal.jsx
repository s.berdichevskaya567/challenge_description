import React from 'react';

const Modal = ({active, setActive, nameImg, sizeImg}) => {

    return (
        <div className={active ?  'modal active' : 'modal'}  onClick={()=>{setActive(false),
            document.querySelector('body').style.overflow = 'auto',
            document.querySelector('body').style.height = 'auto'}}
            onLoad={()=>{document.querySelector('body').style.overflow = 'hidden',
            document.querySelector('body').style.height = '100%'
        }}

        >
            {nameImg !== undefined &&
            <div style={{position: "fixed", overflow: "hidden"}}>
                <div className={active ? 'modal_content active' : 'modal_content'}
                     onClick={(e)=>{e.stopPropagation()}}>
                    <img src={`http://contest.elecard.ru/frontend_data/${nameImg}`}/>
                    <p>Размер картинки: {(Number(sizeImg) / 1000).toFixed(2)}, кБ</p>
                </div>
            </div>
            }
        </div>
    );
};

export default Modal;