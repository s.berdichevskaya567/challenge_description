import React, {useState} from 'react';
import LevelTwo from "./LevelTwo.jsx";

const LevelOne = ({categoryName, card}) => {

    let [openingCardNames, setOpeningCardNames] = useState(false)
    let [open, setOpen] = useState(false)

    const stateOpeningCardNames = () => {
        setOpeningCardNames(!openingCardNames)
        setOpen(!open)
    }



    return (
        <div>
            <div style={{width: '90px', cursor: 'pointer'}} onClick={() => {stateOpeningCardNames()}}>
                {open ? <> &#9660;  </> : <> &#9658;  </>} {categoryName}
            </div>
            {
                (openingCardNames === true) && <LevelTwo className='treeList' card={card} el={categoryName}/>
            }
        </div>
    );
};

export default LevelOne;