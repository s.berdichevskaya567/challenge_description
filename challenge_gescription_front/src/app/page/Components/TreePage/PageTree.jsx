import React, {useState} from 'react';
import {dataFromServer} from "../../../../data/requier.js";
import LevelOne from "./LavelOne.jsx";
import HeaderTree from "./components/Header.jsx";
import FooterTree from "./components/Footer.jsx";

const PageTree = () => {

    const card = dataFromServer();
    let [rootDiscovery, setRootDiscovery] = useState(true)
    let [open, setOpen] = useState(true)

    const stateRootDiscovery = () => {
        setRootDiscovery(!rootDiscovery)
        setOpen(!open)

    }

    const category = card.reduce((acc, item) => {
        if (acc.map[item.category])
            return acc;
        acc.map[item.category] = true
        acc.category.push(item.category)
        return acc
    },
        {
        map: {},
        category: []
    }).category


    return (
        <div>
            <HeaderTree/>
            {open ? <div style={{marginTop: '20px', marginBottom: '-45px'}}> &#9660; </div> : <div style={{marginTop: '20px', marginBottom: '-45px'}}> &#9658; </div>}
            <div className='rootTree' onClick={() => {stateRootDiscovery()}}>
                Категории
            </div>
            {
                (rootDiscovery === true) && category.map((categoryName, index) => {
                    return (
                        <div className='treeList' key={index}>
                            <LevelOne categoryName={categoryName} card={card}/>
                        </div>
                    )
                })
            }
            <FooterTree/>
        </div>
    );
};

export default PageTree;