import React, {useState} from 'react';
import PageCards from "./Components/CardsPage/PageCards.jsx";

import PageTree from "./Components/TreePage/PageTree.jsx";


const Component = () => {

    const [view, setView] = useState('cards')
    const onChangeView = (e) => {
        setView(e.target.name)
    }

    return (
        <div>
            <div className='mode'>
                <p style={{}}>
                    Вид отображения
                </p>
                </div>
            <section className='mode' style={{border: "darkseagreen"}}>
                <div style={{marginRight: "20px"}}>
                    <input type={"radio"}
                           name={'cards'}
                           id={'cards'}
                           onChange={onChangeView}
                           checked={(view === 'cards')}
                    />
                    <label htmlFor={'cards'}
                           className={(view === 'cards')? 'active' : ''}
                    >
                        Карточки
                    </label>
                </div>
                <div>
                    <input type={"radio"}
                           name={'treeList'}
                           id={'treeList'}
                           onChange={onChangeView}
                           checked={(view === 'treeList')}
                    />
                    <label htmlFor={'treeList'}
                           className={(view === 'treeList')? 'active' : ''}
                    >
                        Древовидный список
                    </label>
                </div>

            </section>
            {
                (view === 'cards')
                    ? <PageCards/>
                    : <PageTree/>
            }
        </div>
    );
};

export default Component;